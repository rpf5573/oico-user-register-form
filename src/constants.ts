const NEW_LINE_CHAR = '__NEW_LINE_CHAR__';
const LANDLINE_NUM_LIST = [
  '02',
  '031',
  '032',
  '051',
  '033',
  '041',
  '042',
  '043',
  '044',
  '052',
  '053',
  '054',
  '055',
  '061',
  '062',
  '063',
  '064',
];
const MOBILE_NUM_LIST = ['010', '011'];
const MAIL_LIST = [
  'naver.com',
  'kakao.com',
  'gmail.com',
  'nate.com',
  'yahoo.com',
];
const DAUM_POSTCODE_API_URL =
  '//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js';

export {
  NEW_LINE_CHAR,
  LANDLINE_NUM_LIST,
  MOBILE_NUM_LIST,
  MAIL_LIST,
  DAUM_POSTCODE_API_URL,
};
