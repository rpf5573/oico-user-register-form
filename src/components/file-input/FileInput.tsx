import React, { useRef, useState } from 'react';
import Button from '../button/Button';

type FileInputProps = {
  name: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onDelete: () => void;
  onBlur: () => void;
  errorMessages?: string;
  fileName?: string;
};

const FileInput = ({
  name,
  onChange,
  onDelete,
  onBlur,
  errorMessages,
  fileName,
}: FileInputProps) => {
  const fileInputRef = useRef<HTMLInputElement>(null);
  const [selectedFileName, setSelectedFileName] = useState(fileName || '');

  // Function to open file dialog
  const handleBrowseButtonClick = () => {
    fileInputRef.current?.click();
  };

  // Function to update file name on change
  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      setSelectedFileName(file.name);
    } else {
      setSelectedFileName('');
    }
    onChange(event);
  };

  // Function to clear the selected file
  const handleDeleteButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.value = '';
      setSelectedFileName('');
      onDelete();
    }
  };

  return (
    <div className="file-input-container flex flex-col gap-2 w-full">
      <div className="file-input flex gap-2">
        <input
          type="text"
          readOnly
          value={selectedFileName}
          className="flex-1"
        />
        <input
          type="file"
          ref={fileInputRef}
          name={name}
          onChange={handleFileChange}
          onBlur={onBlur}
          className="!hidden"
        />
        <div className="flex gap-2">
          <Button type="button" onClick={handleBrowseButtonClick}>
            찾아보기
          </Button>
          <Button
            type="button"
            onClick={handleDeleteButtonClick}
            variant="secondary"
          >
            삭제
          </Button>
        </div>
      </div>
      {errorMessages && (
        <div className="error-message flex flex-col gap-1">{errorMessages}</div>
      )}
    </div>
  );
};

export default FileInput;
