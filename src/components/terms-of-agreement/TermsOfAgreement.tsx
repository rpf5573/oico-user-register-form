import { useForm } from 'react-hook-form';
import Checkbox from '../checkbox/Checkbox';
import { z } from 'zod';
import Button from '../button/Button';
import { zodResolver } from '@hookform/resolvers/zod';
import useScrollToTop from '@/hooks/useScrollToTop';

type TermsOfAgreementSchemaType = z.infer<typeof termsOfAgreementSchema>;
const termsOfAgreementSchema = z.object({
  agree1: z
    .boolean()
    .refine((v) => v === true, { message: '동의가 필요합니다' }),
  agree2: z
    .boolean()
    .refine((v) => v === true, { message: '동의가 필요합니다' }),
});

type TermsOfAgreementProps = {
  onSubmit: () => void;
};
const TermsOfAgreement = ({
  onSubmit: _handleSubmit,
}: TermsOfAgreementProps) => {
  const {
    register,
    handleSubmit: handleSubmit,
    formState: { errors },
  } = useForm<TermsOfAgreementSchemaType>({
    resolver: zodResolver(termsOfAgreementSchema),
  });

  useScrollToTop();

  const onSubmit = (data: any) => {
    console.log('data', data);
    _handleSubmit();
  };

  return (
    <div className="terms-of-agreement">
      <h2 className="mb-2">
        산업융합촉진 옴부즈만 홈페이지 회원가입을 위한 약관 동의 입니다.
      </h2>
      <p className="mb-12">
        회원으로 가입하시기 전에 아래의 이용약관을 반드시 읽어주세요.
      </p>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="agree-1 mb-16">
          <h3 className="mb-6 text-xl font-medium">회원약관 확인</h3>
          <div className="content mb-5">
            <h3>서비스 이용약관</h3>
            <p>제정 2003.4.1.</p>

            <h3>[제 1장 총칙]</h3>
            <p>제1조 목적</p>

            <p>
              이 약관은 산업융합촉진 옴부즈만이 정보통신망을 이용하여 제공하는
              서비스를 이용함에 있어 그 산업융합촉진 옴부즈만의 회원의 권리·의무
              및 책임 기타 필요한 사항을 규정함을 목적으로 합니다.
            </p>
          </div>
          <div className="flex justify-center mb-2">
            <Checkbox
              label="이용약관에 동의합니다"
              type="checkbox"
              id="agree-1-checkbox"
              {...register('agree1')}
            />
          </div>
          <div className="error-message flex justify-center">
            {errors.agree1 && errors.agree1.message}
          </div>
        </div>
        <div className="agree-2 mb-16">
          <h3 className="mb-6 text-xl font-medium">
            개인정보보호법 제15조, 제24조에 의한 수집/이용에 동의
          </h3>
          <div className="content mb-5">
            <h3>서비스 이용약관</h3>
            <p>제정 2003.4.1.</p>

            <h3>[제 1장 총칙]</h3>
            <p>제1조 목적</p>

            <p>
              이 약관은 산업융합촉진 옴부즈만이 정보통신망을 이용하여 제공하는
              서비스를 이용함에 있어 그 산업융합촉진 옴부즈만의 회원의 권리·의무
              및 책임 기타 필요한 사항을 규정함을 목적으로 합니다.
            </p>
          </div>
          <div className="flex justify-center mb-2">
            <Checkbox
              label="개인정보보호법 제15조, 제24조에 의한 수집·이용 및 개인정보처리방침에 동의합니다"
              type="checkbox"
              id="agree-2-checkbox"
              {...register('agree2')}
            />
          </div>
          <div className="error-message flex justify-center">
            {errors.agree2 && errors.agree2.message}
          </div>
        </div>
        <div className="flex justify-center">
          <Button
            type="submit"
            variant="primary-gradient"
            className="text-[24px] py-[19px] px-[177px]"
          >
            다음
          </Button>
        </div>
      </form>
    </div>
  );
};

export default TermsOfAgreement;
