import React, { forwardRef } from 'react';

type NumberInputProps = {
  _min?: number;
  _max?: number;
  _minLength?: number;
  _maxLength?: number;
  name: string;
  className?: string;
};

const NumberInput = forwardRef<HTMLInputElement, NumberInputProps>(
  ({ _min, _max, _minLength, _maxLength, className, name }, ref) => {
    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
      let value = Number.parseInt(e.target.value);

      // minLength-maxLength 처리
      if (_minLength && value.toString().length <= _minLength) {
        value = Number(value.toString().padStart(_minLength, '0'));
      } else if (_maxLength && value.toString().length >= _maxLength) {
        value = Number(value.toString().slice(0, _maxLength));
      }

      // min-max 처리
      if (_min && value < _min) {
        value = _min;
      } else if (_max && value > _max) {
        value = _max;
      }

      console.log('value', value);

      e.target.value = value.toString();
    };

    const cn = `number-input ${className}`;

    return (
      <input
        ref={ref} // Attach the forwarded ref to the input
        name={name}
        className={cn}
        type="number"
        min={_min}
        max={_max}
        onInput={handleInput}
      />
    );
  },
);

export default NumberInput;
