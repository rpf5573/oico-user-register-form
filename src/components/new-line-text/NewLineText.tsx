import { NEW_LINE_CHAR } from '@/constants';

type NewLineTextProps = {
  text: string;
};
const NewLineText = ({ text }: NewLineTextProps) => {
  const multipleLineText = text
    .split(NEW_LINE_CHAR)
    .map((str, index) => <div key={index}>{str}</div>);
  return <div className="new-line-text">{multipleLineText}</div>;
};

export default NewLineText;
