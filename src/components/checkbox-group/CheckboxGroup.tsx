type CheckboxGroupProps = {
  children: React.ReactNode;
};
const CheckboxGroup = ({ children }: CheckboxGroupProps) => {
  return <div className="checkbox-group flex gap-6">{children}</div>;
};

export default CheckboxGroup;
