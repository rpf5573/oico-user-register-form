import {
  useForm,
  SubmitHandler,
  FormProvider,
  useFormContext,
  useWatch,
  Controller,
} from 'react-hook-form';

import { z } from 'zod';

import { zodResolver } from '@hookform/resolvers/zod';
import FormField from '@components/form-field/FormField';
import Button from '@components/button/Button';

import {
  DAUM_POSTCODE_API_URL,
  LANDLINE_NUM_LIST,
  MAIL_LIST,
  MOBILE_NUM_LIST,
  NEW_LINE_CHAR,
} from '@constants';
import { useDaumPostcodePopup } from 'react-daum-postcode';
import { useEffect, useState } from 'react';
import NumberInput from '@components/number-input/NumberInput';
import clsx from 'clsx';
import FileInput from '@components/file-input/FileInput';
import useScrollToTop from '@/hooks/useScrollToTop';

// 용어 정리
// 접수일: receiptDate;
// 신청기업기관: applicantCompany_institution;
// 담당자1: contactPerson1;
// 담당자2: contactPerson2;
// 연락처: contactNumber;
// 이메일: email;
// 품목명: itemName;
// 제목: title;
// 공개여부: isPublic;

type RegisterSchemaType = z.infer<typeof registerSchema>;

const MAX_FILE_SIZE = 2000000;

const registerSchema = z.object({
  name: z.string().min(1, { message: '이름을 입력해주세요.' }),
  userId: z.string().min(1, { message: '사용자 ID를 입력해주세요.' }),
  password: z
    .string()
    .min(6, { message: '비밀번호는 6자 이상이어야 합니다.' })
    .refine((val) => /^[a-z0-9]+$/.test(val), {
      message: '비밀번호는 영문 소문자와 숫자만 포함할 수 있습니다.',
    }),
  confirmPassword: z
    .string()
    .min(6, { message: '비밀번호 확인이 필요합니다.' }),
  companyName: z.string().min(1, { message: '회사명을 입력해주세요.' }),
  businessRegNumber1: z
    .string()
    .min(1, { message: '사업자등록번호를 입력해주세요.' }),
  businessRegNumber2: z
    .string()
    .min(1, { message: '사업자등록번호를 입력해주세요.' }),
  businessRegNumber3: z
    .string()
    .min(1, { message: '사업자등록번호를 입력해주세요.' }),
  representativeName: z
    .string()
    .min(1, { message: '대표자명을 입력해주세요.' }),
  contactPersonName: z.string().min(1, { message: '담당자명을 입력해주세요.' }),
  landlinePhone1: z.string().min(1, { message: '일반전화를 입력해주세요.' }),
  landlinePhone2: z.string().min(1, { message: '일반전화를 입력해주세요.' }),
  landlinePhone3: z.string().min(1, { message: '일반전화를 입력해주세요.' }),
  mobilePhone1: z.string().min(1, { message: '휴대전화 번호를 입력해주세요.' }),
  mobilePhone2: z.string().min(1, { message: '휴대전화 번호를 입력해주세요.' }),
  mobilePhone3: z.string().min(1, { message: '휴대전화 번호를 입력해주세요.' }),
  companyLandlinePhone1: z
    .string()
    .min(1, { message: '일반전화를 입력해주세요.' }),
  companyLandlinePhone2: z
    .string()
    .min(1, { message: '일반전화를 입력해주세요.' }),
  companyLandlinePhone3: z
    .string()
    .min(1, { message: '일반전화를 입력해주세요.' }),
  companyMobilePhone1: z
    .string()
    .min(1, { message: '휴대전화 번호를 입력해주세요.' }),
  companyMobilePhone2: z
    .string()
    .min(1, { message: '휴대전화 번호를 입력해주세요.' }),
  companyMobilePhone3: z
    .string()
    .min(1, { message: '휴대전화 번호를 입력해주세요.' }),
  email1: z.string().email({ message: '유효한 이메일 주소를 입력해주세요.' }),
  email2: z.string().email({ message: '유효한 이메일 주소를 입력해주세요.' }),
  zonecode: z.string().min(1, { message: '우편번호를 입력해주세요.' }),
  address: z.string().min(1, { message: '주소를 입력해주세요.' }),
  extraAddress: z.string().optional(),
  businessRegistration: z
    .any()
    .optional()
    .refine((file?: File) => {
      if (!file) return true;
      if (!(file instanceof File)) return true;
      return file.size <= MAX_FILE_SIZE;
    }, `2MB이하의 파일만 허용됩니다`),
});

type RegistrationFormProps = {
  onCancel: () => void;
};
function RegistrationForm({ onCancel: handleCancel }: RegistrationFormProps) {
  const formMethods = useForm<RegisterSchemaType>({
    resolver: zodResolver(registerSchema),
    defaultValues: {
      name: '',
      userId: '',
      password: '',
      confirmPassword: '',
    },
  });

  useScrollToTop();

  const onSubmit: SubmitHandler<RegisterSchemaType> = (data) => {
    console.log('data', data);
  };

  return (
    <FormProvider {...formMethods}>
      <p className="mb-12">
        <b>산업융합촉진 옴부즈만 홈페이지 회원가입 시 입력하신 회원정보</b>는
        <br />
        개인정보취급방침에 따라 안전하게 보호되며 회원님의 동의없이 공개 또는
        제3자에게 제공되지 않습니다.
      </p>
      <div className="form-container">
        <form onSubmit={formMethods.handleSubmit(onSubmit)}>
          <section className="mb-14">
            <h2 className="font-bold mb-3">
              <span>기본정보 입력</span>
            </h2>
            <div className="om-divider mb-4"></div>
            <div className="form-fields">
              {/* 이름 */}
              <div className="om-form-row flex">
                <NameField />
              </div>

              {/* 아이디 */}
              <div className="om-form-row flex">
                <IdField />
              </div>

              {/* 비밀번호 */}
              <div className="om-form-row flex">
                <PasswordField />
              </div>

              {/* 비밀번호 확인 */}
              <div className="om-form-row flex">
                <ConfirmPasswordField />
              </div>

              {/* 일반전화 */}
              <div className="om-form-row flex">
                <LandlinePhoneField />
              </div>

              {/* 휴대전화 */}
              <div className="om-form-row flex">
                <MobilePhoneField />
              </div>

              {/* 이메일 */}
              <div className="om-form-row flex">
                <EmailField />
              </div>

              {/* 주소 */}
              <div className="om-form-row flex">
                <AddressField />
              </div>
            </div>
          </section>

          <section className="mb-14">
            <h2 className="font-bold mb-3">
              <span>기업(기관)정보 입력</span>
            </h2>
            <div className="om-divider mb-4"></div>
            <div className="form-fields">
              {/* 회사명 */}
              <div className="om-form-row flex">
                <CompanyNameField />
              </div>

              {/* 대표자명 */}
              <div className="om-form-row flex">
                <RepresentativeNameField />
              </div>

              {/* 사업자등록번호 */}
              <div className="om-form-row flex">
                <BusinessRegNumField />
              </div>

              {/* 사업자등록증 첨부 */}
              <div className="om-form-row flex">
                <BusinessRegCertAttachmentField />
              </div>
            </div>
          </section>

          <section className="mb-20">
            <h2 className="font-bold mb-3">
              <span>기업(기관)담당자 정보</span>
            </h2>
            <div className="om-divider mb-4"></div>
            <div className="form-fields">
              {/* 담당자명 */}
              <div className="om-form-row flex">
                <ContactPersonNameField />
              </div>

              {/* 일반전화 */}
              <div className="om-form-row flex">
                <CompanyLandlinePhoneField />
              </div>

              {/* 휴대전화 */}
              <div className="om-form-row flex">
                <CompanyMobilePhoneField />
              </div>

              {/* 이메일 */}
              <div className="om-form-row flex">
                <EmailField />
              </div>
            </div>
          </section>

          <div className="flex justify-center gap-12">
            <Button
              variant="secondary"
              className="w-[245px]"
              onClick={handleCancel}
            >
              취소
            </Button>
            <Button
              type="submit"
              variant="primary-gradient"
              className="w-[245px]"
            >
              회원가입
            </Button>
          </div>
        </form>
      </div>
    </FormProvider>
  );
}

export default RegistrationForm;

// 이름필드
const NameField = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext<RegisterSchemaType>();

  return (
    <FormField
      label="이름"
      valueContent={
        <input
          type="text"
          {...register('name')}
          id="name"
          className="w-[240px]"
        />
      }
      errorMessage={errors.name?.message}
    />
  );
};

// 아이디 필드
const IdField = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext<RegisterSchemaType>();

  return (
    <FormField
      label="사용자 ID"
      valueContent={
        <div className="flex">
          <div>
            <input
              type="text"
              {...register('userId')}
              id="userId"
              className="w-[240px]"
            />
          </div>
          <div className="ml-2">
            <Button variant="secondary">중복확인</Button>
          </div>
        </div>
      }
      errorMessage={errors.userId?.message}
    />
  );
};

// 비밀번호 필드
const PasswordField = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext<RegisterSchemaType>();

  return (
    <FormField
      label="비밀번호"
      valueContent={
        <input
          type="password"
          {...register('password')}
          id="password"
          className="w-[240px]"
        />
      }
      className={errors.password ? 'error' : ''}
      noticeMessage={`비밀번호는 6자 이상 영문(소문)과 숫자를 혼용하여야 하며,${NEW_LINE_CHAR}!, @, #, $, %, &, * 등의 특수문자는 사용하실 수 없습니다.`}
    />
  );
};

// 비밀번호 확인 필드
const ConfirmPasswordField = () => {
  const {
    register,
    formState: { errors },
    getValues,
    watch,
    setError,
    clearErrors,
  } = useFormContext<RegisterSchemaType>();

  const confirmPassword = watch('confirmPassword');
  useWatch({
    name: 'password',
    defaultValue: '',
  });

  const password = getValues('password');

  useEffect(() => {
    if (confirmPassword && password !== confirmPassword) {
      setError('confirmPassword', {
        type: 'validate',
        message: '비밀번호가 일치하지 않습니다.',
      });
      return;
    }
    clearErrors('confirmPassword');
  }, [password, confirmPassword]);

  return (
    <FormField
      label="비밀번호 확인"
      valueContent={
        <div className="flex items-center">
          <div>
            <input
              type="password"
              {...register('confirmPassword')}
              id="confirmPassword"
              className="w-[240px]"
            />
          </div>
          <div className="ml-2 flex">
            <span className="text-gray-900">
              비밀번호를 한번 더 입력해 주십시오.
            </span>
          </div>
        </div>
      }
      className={errors.confirmPassword ? 'error' : ''}
      errorMessage={errors.confirmPassword?.message}
    />
  );
};

// 일반전화 필드
const LandlinePhoneField = () => {
  const { register, getValues } = useFormContext<RegisterSchemaType>();
  useWatch({
    name: 'landlinePhone1',
    defaultValue: '',
  });
  const selectedValue = getValues('landlinePhone1');

  const selectClassName = clsx({
    '!text-gray-900': selectedValue === '',
  });

  return (
    <FormField
      label="일반전화"
      valueContent={
        <div className="flex items-center gap-2">
          <div className="flex">
            <select {...register('landlinePhone1')} className={selectClassName}>
              <option value="">직접입력</option>
              {LANDLINE_NUM_LIST.map((num, index) => (
                <option value={num} key={`${num}-${index}`}>
                  {num}
                </option>
              ))}
            </select>
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={4}
              {...register('landlinePhone2')}
              className="w-[100px]"
            />
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={4}
              {...register('landlinePhone3')}
              className="w-[100px]"
            />
          </div>
        </div>
      }
    />
  );
};

// 휴대전화 필드
const MobilePhoneField = () => {
  const { register, getValues } = useFormContext<RegisterSchemaType>();
  useWatch({
    name: 'mobilePhone1',
    defaultValue: '',
  });
  const selectedValue = getValues('mobilePhone1');
  const selectClassName = clsx({
    '!text-gray-900': selectedValue === '',
  });

  return (
    <FormField
      label="휴대전화"
      valueContent={
        <div className="flex items-center gap-2">
          <div className="flex">
            <select {...register('mobilePhone1')} className={selectClassName}>
              <option value="">직접입력</option>
              {MOBILE_NUM_LIST.map((num, index) => (
                <option value={num} key={`${num}-${index}`}>
                  {num}
                </option>
              ))}
            </select>
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={4}
              {...register('mobilePhone2')}
              className="w-[100px]"
            />
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={4}
              {...register('mobilePhone3')}
              className="w-[100px]"
            />
          </div>
        </div>
      }
    />
  );
};

// 이메일 필드
const EmailField = () => {
  const { register, setValue } = useFormContext<RegisterSchemaType>();
  const [email2, setEmail2] = useState('');
  const [isEmail2Hidden, setIsEmail2Hidden] = useState(false);

  useEffect(() => {
    setValue('email2', email2);
  }, [email2]);

  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const val = e.target.value;
    // 직접입력을 선택하면 input을 활성화
    if (val === '') {
      setEmail2('');
      setIsEmail2Hidden(false);
    } else {
      setEmail2(val);
      setIsEmail2Hidden(true);
    }
  };

  const email2InputClassName = clsx({
    'w-[150px]': true,
    '!hidden': isEmail2Hidden,
  });

  const selectClassName = clsx({
    '!text-gray-900': email2 === '', // 직접입력 모드라는 의미입니다
  });

  return (
    <FormField
      label="이메일"
      valueContent={
        <div>
          <div className="flex items-center gap-2 mb-2">
            <div>
              <input
                type="text"
                className="w-[150px]"
                {...register('email1')}
              />
            </div>
            <div className="text-gray-900">@</div>
            <div className="flex items-center gap-2">
              <input
                type="text"
                className={email2InputClassName}
                {...register('email2')}
              />
              <select
                id=""
                onChange={handleSelectChange}
                className={selectClassName}
              >
                <option value="">직접입력</option>
                {MAIL_LIST.map((mail, index) => (
                  <option value={mail} key={`${mail}-${index}`}>
                    {mail}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <div className="">
            <p className="text-gray-900">
              hanmail, hotmail 은 사용하실 수 없습니다.
            </p>
          </div>
        </div>
      }
    />
  );
};

// 주소 필드
const AddressField = () => {
  const { register, setValue } = useFormContext<RegisterSchemaType>();
  const [zonecode, setZonecode] = useState('');
  const [address, setAddress] = useState('');

  useEffect(() => {
    setValue('zonecode', zonecode);
    setValue('address', address);
  }, [zonecode, address]);

  const open = useDaumPostcodePopup(DAUM_POSTCODE_API_URL);

  const extractAddress = (data: any) => {
    // 각 주소의 노출 규칙에 따라 주소를 조합한다.
    // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
    const zonecode = data.zonecode;
    let addr = ''; // 주소 변수
    let extraAddr = ''; // 참고항목 변수

    //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
    if (data.userSelectedType === 'R') {
      // 사용자가 도로명 주소를 선택했을 경우
      addr = data.roadAddress;
    } else {
      // 사용자가 지번 주소를 선택했을 경우(J)
      addr = data.jibunAddress;
    }

    // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
    if (data.userSelectedType === 'R') {
      // 법정동명이 있을 경우 추가한다. (법정리는 제외)
      // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
      if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
        extraAddr += data.bname;
      }
      // 건물명이 있고, 공동주택일 경우 추가한다.
      if (data.buildingName !== '' && data.apartment === 'Y') {
        extraAddr +=
          extraAddr !== '' ? ', ' + data.buildingName : data.buildingName;
      }
      // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
      if (extraAddr !== '') {
        extraAddr = ' (' + extraAddr + ')';
      }
      // 조합된 참고항목을 해당 필드에 넣는다.
    }

    return {
      zonecode,
      address: addr + extraAddr,
    };
  };

  const handleComplete = (data: any) => {
    const { zonecode, address } = extractAddress(data);
    setZonecode(zonecode);
    setAddress(address);
  };

  const handleClick = () => {
    open({ onComplete: handleComplete });
  };

  return (
    <FormField
      label="주소"
      valueContent={
        <div>
          <div className="flex items-center mb-2">
            <div>
              <input
                type="text"
                {...register('zonecode')}
                className="w-[240px]"
              />
            </div>
            <div className="ml-2">
              <Button variant="secondary" onClick={handleClick}>
                주소찾기
              </Button>
            </div>
          </div>
          <div className="mb-2">
            <input
              type="text"
              className="w-[620px]"
              {...register('address')}
              placeholder="우편번호 검색을 통해 자동 입력됩니다."
            />
          </div>
          <div>
            <input
              type="text"
              className="w-[620px]"
              {...register('extraAddress')}
              placeholder="나머지 주소를 직접 입력해주세요."
            />
          </div>
        </div>
      }
    />
  );
};

// 회사명 필드
const CompanyNameField = () => {
  const { register } = useFormContext<RegisterSchemaType>();

  return (
    <FormField
      label="회사명"
      valueContent={
        <input type="text" {...register('companyName')} className="w-[240px]" />
      }
    />
  );
};

// 사업자등록번호 필드
const BusinessRegNumField = () => {
  const { register } = useFormContext<RegisterSchemaType>();

  return (
    <FormField
      label="사업자등록번호"
      valueContent={
        <div className="flex items-center gap-2">
          <div className="flex">
            <NumberInput
              _minLength={2}
              _maxLength={3}
              {...register('businessRegNumber1')}
              className="w-[100px]"
            />
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={2}
              _maxLength={3}
              {...register('businessRegNumber2')}
              className="w-[110px]"
            />
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={5}
              {...register('businessRegNumber3')}
              className="w-[150px]"
            />
          </div>
        </div>
      }
    />
  );
};

// 대표자명 필드
const RepresentativeNameField = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext<RegisterSchemaType>();

  return (
    <FormField
      label="대표자명"
      valueContent={
        <input
          type="text"
          {...register('representativeName')}
          className="w-[240px]"
        />
      }
      errorMessage={errors.representativeName?.message}
    />
  );
};

// 담당자명 필드
const ContactPersonNameField = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext<RegisterSchemaType>();

  return (
    <FormField
      label="담당자명"
      valueContent={
        <input
          type="text"
          {...register('contactPersonName')}
          className="w-[240px]"
        />
      }
      errorMessage={errors.contactPersonName?.message}
    />
  );
};

// 사업자등록증첨부 필드
const BusinessRegCertAttachmentField = () => {
  const {
    formState: { errors },
    control,
  } = useFormContext<RegisterSchemaType>();

  return (
    <FormField
      label="사업자등록증첨부"
      valueContent={
        <div className="file-input-set tw-flex tw-gap-2 min-w-[620px]">
          <Controller
            name="businessRegistration"
            control={control}
            render={({
              field: { onChange, onBlur, name },
              fieldState: { error },
            }) => (
              <FileInput
                name={name}
                onChange={(e) => {
                  onChange([...(e.target.files ?? [])][0]);
                }}
                onDelete={() => {
                  onChange(undefined);
                }}
                onBlur={onBlur}
                errorMessages={error?.message}
              />
            )}
          />
        </div>
      }
      noticeMessage={`기업회원일 경우 사업자등록증 파일을 필히 첨부해 주시기 바랍니다.${NEW_LINE_CHAR}첨부파일 용량은 2M 미만의 파일로 올려주시기 바랍니다.`}
      errorMessage={errors.businessRegistration?.message?.toString()}
    />
  );
};

// 일반전화 필드
const CompanyLandlinePhoneField = () => {
  const { register, getValues } = useFormContext<RegisterSchemaType>();
  useWatch({
    name: 'companyLandlinePhone1',
    defaultValue: '',
  });
  const selectedValue = getValues('companyLandlinePhone1');
  const selectClassName = clsx({
    '!text-gray-900': selectedValue === '',
  });

  return (
    <FormField
      label="일반전화"
      valueContent={
        <div className="flex items-center gap-2">
          <div className="flex">
            <select
              {...register('companyLandlinePhone1')}
              className={selectClassName}
            >
              <option value="">직접입력</option>
              {LANDLINE_NUM_LIST.map((num, index) => (
                <option value={num} key={`${num}-${index}`}>
                  {num}
                </option>
              ))}
            </select>
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={4}
              {...register('companyLandlinePhone2')}
              className="w-[100px]"
            />
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={4}
              {...register('companyLandlinePhone3')}
              className="w-[100px]"
            />
          </div>
        </div>
      }
    />
  );
};

// 휴대전화 필드
const CompanyMobilePhoneField = () => {
  const { register, getValues } = useFormContext<RegisterSchemaType>();
  useWatch({
    name: 'companyMobilePhone1',
    defaultValue: '',
  });
  const selectedValue = getValues('companyMobilePhone1');
  const selectClassName = clsx({
    '!text-gray-900': selectedValue === '',
  });

  return (
    <FormField
      label="휴대전화"
      valueContent={
        <div className="flex items-center gap-2">
          <div className="flex">
            <select
              {...register('companyMobilePhone1')}
              className={selectClassName}
            >
              <option value="">직접입력</option>
              {MOBILE_NUM_LIST.map((num, index) => (
                <option value={num} key={`${num}-${index}`}>
                  {num}
                </option>
              ))}
            </select>
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={4}
              {...register('companyMobilePhone2')}
              className="w-[100px]"
            />
          </div>
          <div className="text-gray-900">-</div>
          <div>
            <NumberInput
              _minLength={3}
              _maxLength={4}
              {...register('companyMobilePhone3')}
              className="w-[100px]"
            />
          </div>
        </div>
      }
    />
  );
};
