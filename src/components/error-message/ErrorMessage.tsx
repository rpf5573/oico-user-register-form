import NewLineText from '../new-line-text/NewLineText';

const ErrorMessage = ({ message }: { message: string }) => {
  return (
    <div className="error-message">
      <span>
        <NewLineText text={message} />
      </span>
    </div>
  );
};

export default ErrorMessage;
