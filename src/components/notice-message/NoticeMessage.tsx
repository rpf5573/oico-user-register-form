import NewLineText from '../new-line-text/NewLineText';

type NoticeMessageProps = {
  message: string;
};
const NoticeMessage = ({ message }: NoticeMessageProps) => {
  return (
    <div className="text-sm text-primary">
      <p>
        <NewLineText text={message} />
      </p>
    </div>
  );
};

export default NoticeMessage;
