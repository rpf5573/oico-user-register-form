import clsx from 'clsx';

type ButtonProps = {
  children: React.ReactNode;
  className?: string;
  onClick?: () => void;
  type?: 'button' | 'submit' | 'reset';
  long?: boolean;
  variant?: 'primary' | 'secondary' | 'tertiary' | 'primary-gradient';
};

const Button = ({
  children,
  className,
  onClick,
  type = 'button',
  long = false,
  variant = 'primary',
}: ButtonProps) => {
  const cn = clsx(
    'text-center',
    'font-semibold',
    'rounded',
    'text-white',
    'py-2 px-7',
    'flex items-center justify-center',
    'leading-[36px]',
    {
      'block w-[400px]': long === true,
    },
    {
      'bg-blue-800 hover:bg-blue-700': variant === 'primary',
      'bg-gray-900 hover:bg-gray-800': variant === 'secondary',
      'bg-gradient-primary': variant === 'primary-gradient',
    },
    className,
  );

  return (
    <button type={type} onClick={onClick} className={cn}>
      {children}
    </button>
  );
};

export default Button;
