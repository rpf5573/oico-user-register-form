import React from 'react';
import './checkbox.scss';

type CheckboxProps = {
    id: string;
    label: string;
    checked?: boolean;
    name: string;
    type: 'checkbox' | 'radio';
    className?: string;
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const Checkbox = React.forwardRef<HTMLInputElement, CheckboxProps>(
  ({ id, label, className, name, checked, type, onChange: handleChange }, ref) => {
    return (
        <div className={`om-checkbox ${className ?? ''}`}>
            <input ref={ref} id={id} type={type} checked={checked} onChange={handleChange} name={name} />
            <label htmlFor={id}>{label}</label>
        </div>
    );
});

export default Checkbox;