import clsx from 'clsx';
import NoticeMessage from '@components/notice-message/NoticeMessage';
import ErrorMessage from '../error-message/ErrorMessage';

type FormField = {
  label: string | React.ReactNode;
  valueContent: React.ReactNode;
  className?: string;
  id?: string;
  noticeMessage?: string;
  errorMessage?: string;
};
const FormField = ({
  id,
  label,
  valueContent,
  className,
  noticeMessage,
  errorMessage,
}: FormField) => {
  const cn = clsx('field flex items-center w-full', className);
  const gap = clsx({
    'mb-2': noticeMessage || errorMessage,
  });

  return (
    <div className="field-container">
      <div className={cn}>
        <div className="left">
          <label htmlFor={id}>{label}</label>
        </div>
        <div className="right flex-1">
          <div className={gap}>{valueContent}</div>
          <div>
            {noticeMessage && <NoticeMessage message={noticeMessage} />}
            {errorMessage && <ErrorMessage message={errorMessage} />}
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormField;
