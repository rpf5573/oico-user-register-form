import '@styles/index.css';
import '@styles/style.scss';
import RightArrow from '@components/RightArrow';
import { useState } from 'react';
import clsx from 'clsx';
import TermsOfAgreement from './components/terms-of-agreement/TermsOfAgreement';
import RegistrationForm from './components/registration-form/RegistrationForm';

const App = () => {
  const [currentStep, setCurrentStep] = useState(0);

  // 동의하면 다음단계로 넘어갑니다
  const handleSubmitTermsOfAgreement = () => {
    setCurrentStep(1);
  };

  const handleCancel = () => {
    setCurrentStep(0);
  };

  return (
    <div id="oico-root-user-register">
      <h1 className="mb-4">회원가입</h1>
      <div className="divider mb-9"></div>
      <div className="mb-6">
        <Steps currentStep={currentStep} />
      </div>
      <main>
        {(() => {
          if (currentStep === 0) {
            return <TermsOfAgreement onSubmit={handleSubmitTermsOfAgreement} />;
          } else if (currentStep === 1) {
            return <RegistrationForm onCancel={handleCancel} />;
          }
        })()}
      </main>
    </div>
  );
};

export default App;

type StepsProps = {
  currentStep: number;
};
const Steps = ({ currentStep }: StepsProps) => {
  return (
    <div className="steps-container mb-6">
      <ul className="steps bg-gradient-primary">
        <li className={clsx({ active: currentStep === 0 })}>약관동의</li>
        <li>
          <RightArrow />
        </li>
        <li className={clsx({ active: currentStep === 1 })}>회원정보 입력</li>
        <li>
          <RightArrow />
        </li>
        <li className={clsx({ active: currentStep === 2 })}>회원가입 완료</li>
      </ul>
    </div>
  );
};
