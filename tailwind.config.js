// tailwind.config.js

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#1A9FDC',
        blue: {
          700: '#42afdf',
          800: '#189FDB',
          900: '#1A9FDC',
        },
        gray: {
          800: '#aeaeae',
          850: '#F8F8F8',
          900: '#9F9F9F',
        },
      },
    },
  },
  plugins: [],
};
